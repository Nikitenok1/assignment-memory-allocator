#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

void test1(){
    void *a = _malloc(20);
    printf("\nSimple allocation\n");
    debug_heap(stdout, HEAP_START);
    _free(a);
}

void test2()
{
    void *a = _malloc(20);
    void *b = _malloc(30);
    void *c = _malloc(40);
    _free(b);
    printf("\nFreeing block\n");
    debug_heap(stdout, HEAP_START);
    _free(a);
    _free(c);
}

void test3(){
    void *a = _malloc(20);
    void *b = _malloc(30);
    void *c = _malloc(30);
    void *d = _malloc(40);
    _free(c);
    _free(b);
    printf("\nFreeing multiple blocks\n");
    debug_heap(stdout, HEAP_START);
    _free(a);
    _free(c);
    _free(d);
}

void test4(){
    void *a = _malloc(8000);
    printf("\nAllocating big chunk\n");
    debug_heap(stdout, HEAP_START);
    _free(a);
}

void test5(){
    debug_alloc_page_after(10000);
    void *a = _malloc(6000);
    void *b = _malloc(6000);
    printf("\nAllocating new page\n");
    debug_heap(stdout, HEAP_START);
    _free(a);
    _free(b);
}

int main(){
    
    heap_init(500);

    void (*tests[6]) () = {test1, test2, test3, test4, test5};

    for(long unsigned int i=0; i<5; i++){
        tests[i]();
    }

    return 0;
}